using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ConsoleBase : MonoBehaviour
{
    protected string lastResponse = "";
    public GUIStyle textStyle = new GUIStyle();
    protected Texture2D lastResponseTexture;
	bool done = false;

    protected Vector2 scrollPosition = Vector2.zero;

	private void OnInitComplete()
	{
		Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
	}
	
	private void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}

	private void CallFBInit()
	{
		FB.Init(OnInitComplete, OnHideUnity);
	}

	void Update(){
		if (FB.IsLoggedIn && !done) {
			done = true;
			try {
				CallFBFeed ();
			} catch (Exception e) {} }

	}

	void LoginCallback(FBResult result)
	{
		if (result.Error != null)
			lastResponse = "Error Response:\n" + result.Error;
		else if (!FB.IsLoggedIn)
		{
			lastResponse = "Login cancelled by Player";
		}
		else
		{
			lastResponse = "Login was successful!";
		}
	}

	private void CallFBLogin()
	{
		FB.Login("public_profile,email,user_friends", LoginCallback);
	}
	
	private void CallFBFeed()
	{
		FB.Feed(
			link: "http://www.facebook.com",
			linkName: "Facebook!",
			linkCaption: "This is a caption.",
			linkDescription: "This is a description.",
			picture:"https://pbs.twimg.com/profile_images/474263905594650624/mExrCKGi_400x400.png",
			callback: Callback
			);
	}

    protected void Callback(FBResult result)
    {
        lastResponseTexture = null;
        // Some platforms return the empty string instead of null.
        if (!String.IsNullOrEmpty (result.Error))
        {
            lastResponse = "Error Response:\n" + result.Error;
        }
        else if (!String.IsNullOrEmpty (result.Text))
        {
            lastResponse = "Success Response:\n" + result.Text;
        }
        else if (result.Texture != null)
        {
            lastResponseTexture = result.Texture;
            lastResponse = "Success Response: texture\n";
        }
        else
        {
            lastResponse = "Empty Response\n";
        }
    }

	void Awake (){
		done = false;
		if (!FB.IsInitialized) { CallFBInit(); }
	}

   public void asd()
    {
		done = false;
		if (!(FB.IsLoggedIn)) {
			CallFBLogin ();
		}
	}
}
